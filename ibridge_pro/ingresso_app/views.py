import pyticketswitch
from dateutil import utils
import threading
import requests

from pyticketswitch import Client
from pyticketswitch.event import Event
from pyticketswitch.performance import Performance
from pyticketswitch.price_band import PriceBand
from pyticketswitch.discount import Discount
from pyticketswitch.ticket_type import TicketType
from django.conf import settings
from pyticketswitch.customer import Customer
from pyticketswitch.payment_methods import RedirectionDetails
from pyticketswitch.reservation import Reservation
from pyticketswitch.seat import Seat

import uuid

import dateutil.parser
import datetime


class Ingresso(object):

    def __init__(self):
        client = Client('demo', 'demopass')
        self.client = client

    def sort_performance_by_date(performances: Performance) -> Performance:
        """
        For sort perfomance by date in ascending order.

        :return: 2020, 2021, 2020 year wise
        """
        return performances.sort(reverse=True, key=lambda x: datetime.time.strftime(x['date_time'],
                                                                                    '%Y,%m,%d,%H,%M'))

    def get_events_list(self, keywords=None, country_code=None, start_date=None, end_date=None,
                        airport_code=None, circle=None, city_code=None, include_dead=False, offers_only=None,
                        page_length=0, page_number=0, sort_order=None, latitude=None,
                        longitude=None, radius=None, **kwargs):

        """
        This methods is for get all events.

        keywords	    Space separated list of search keywords, e.g. lion king new york or paris tours.
        date_range	    Date range in the form yyyymmdd:yyyymmdd (both are optional) to filter events to those with
                        performances within the date range.
        airport_code	Return events near an airport (specified using an IATA airport code)
        circle	        Return events within in a circular geographical area. Three colon-separated values are needed
                        for latitude, longitude, and radius in kilometres. Example: 51.52961137:-0.10601562:10.
        city_code	    Return events in a particular city. The list of city codes can be retrieved using the
                        cities resource.
        country_code	2-digit country code (using ISO 3166-1 alpha-2).
        include_dead	Include dead events in the results. This could be useful if you dynamically retrieve the list
                        of events from Ingresso and want to continue to display an event page after an event dies,
                        for example to help with search engine optimisation.
        offers_only	    If set to true the response will only include events with a special offer. Note that we rely on
                        cached data, so we cannot guarantee complete accuracy.
        page_length	    Length of a page, default 50.
        page_number	    Zero-indexed page number, default 0. Should be used in conjunction with the page_length parameter.
        sort_order	    Valid values are most_popular (based on sales across all partners over the last 48 hours),
                        alphabetic, cost_ascending (lowest price first, based on the minimum total price
                        [seatprice + surcharge] for the event), cost_descending (highest price first, based on the
                        maximum total price for the event), critic_rating (average critic rating, highest to lowest),
                        recent, last_sale. When there is a tie alphabetic ordering is used to break the tie.
                        Note: there is a slight performance impact to using cost_ascending or cost_descending if
                        you are not also using the req_cost_range parameter.
        """
        date_range = ''
        # formate yyyymmdd:yyyymmdd
        # if start_date == '' && end_date == '':

        # client = Client('demo', 'demopass')

        params = {}

        if keywords:
            params.update(keywords=','.join(keywords))

        if start_date or end_date:
            new_s_date = dateutil.parser.parse(start_date).date()
            new_e_date = dateutil.parser.parse(end_date).date()

            if new_e_date < new_s_date:
                print('End date must be higher then start date')
                # return HttpResponse({
                #  'success': False,
                # 'error': 'End date must be higher then start date',
                # })
            date_range = new_s_date, ':', new_e_date
            print(date_range)

            params.update(date_range=dateutil.parser.date_range_str(start_date, end_date))

        if country_code:
            params.update(country_code=country_code)

        if city_code:
            params.update(city_code=city_code)

        if all([latitude, longitude, radius]):
            params.update(circle='{lat}:{lon}:{rad}'.format(
                lat=latitude,
                lon=longitude,
                rad=radius,
            ))
        elif any([latitude, longitude, radius]):
            raise requests.exceptions.InvalidGeoParameters(
                'Geo data must include latitude, longitude, and radius',
            )
        if include_dead:
            params.update(include_dead=True)

        if sort_order:
            params.update(sort_order=sort_order)

        if page_number > 0:
            params.update(page_no=page_number)
        if page_length > 0:
            params.update(page_len=page_length)

        events, meta = self.client.list_events(params)

        print('meta', meta.__dict__)
        # print('Meta Data: %s', meta)

        for event in events:
            # print('meta11', len(event['APIError']))
            print('Event Data:', str(event.__dict__))
            print('\n')
            print(event)

        # return HttpResponse({
        #   'success': True,
        #  'events': events,
        # })

        # return HttpResponse(str(events[0].__dict__))

    def get_events_by_eventid(self, event_id1='7AB', event_id2='6IF'):
        """
        This method is for get event details using event ID.
        In this method we can pass multiple key with comma separated form
        """

        events, meta = self.client.get_events(
            event_ids=[event_id1, event_id2],
            with_addons=True,
            with_upsells=True,
        )
        print("Event list by Event id:", str(events[event_id1].__dict__))
        # return HttpResponse({
        #   'success': True,
        #  'events': events,
        # })

    def get_events_by_id_with_media(self, event_id='6IF', media=True):
        """
        This method is for get event details including media using event ID.
        """
        # events, meta = client.get_events(event_id, media=True)
        events = self.__get_event_by_id(event_id, media=False)
        print(events)
        # return HttpResponse({
        #   'success': True,
        #  'events': events,
        # })

    def __get_event_by_id(self, event_id, media=True):
        """
        This is private method for get event details with different parameters using event id.
        """
        #  client = Client('demo', 'demopass')

        return self.client.get_events(event_id, media=True)

    def get_events_by_id_with_extra_info(self, event_id='6IF', extra_info=True):
        """
        This method is for get event details including Extra info using event ID.
        """

        events, meta = self.__get_event_by_id(event_id, extra_info=extra_info)
        print(str(events[event_id].__dict__))
        # return HttpResponse({
        #   'success': True,
        #  'events': events,
        # })

    def get_events_by_id_with_review(self, event_id='6IF', reviews=True):
        """
        This method is for get event details including Review using event ID.
        """
        events, meta = self.__get_event_by_id(event_id, reviews=reviews)
        print(str(events[event_id].__dict__))
        # return HttpResponse({
        #  'success': True,
        # 'events': events,
        # })

    def get_events_by_id_with_cost_range_details(self, event_id='6IF', cost_range_details=True):
        """
        This method is for get event details including cost range details using event ID.
        """
        events, meta = self.__get_event_by_id(event_id, cost_range_details=cost_range_details)
        print(str(events[event_id].__dict__))
        # return HttpResponse({
        #   'success': True,
        #  'events': events,
        # })

    def get_performances_list_by_event(self, event_id='7AB'):
        """
        This method is for get all performances list using event id.
        """
        # client = Client('demo', 'demopass')
        performances, meta = self.client.list_performances(event_id=event_id)

        for performance in performances:
            print('Performance Data:', str(performance.__dict__))
            print('performance id:- ', str(performance.date_description))
            print('\n')
            print(performance)
        # return HttpResponse({
        #   'success': True,
        #  'performances': performances,
        # })

    def get_performances_by_id(self, performance_id1='7AB-8'):
        """
        This method is for get performances details using performance id.
        """
        # client = Client('demo', 'demopass')
        performances, meta = self.client.get_performances([performance_id1], availability=True)
        for performance in performances:
            print(performance)
        # print(performances.availability_details[0])
        # return HttpResponse({
        #  'success': True,
        # 'performances': performances,
        # })

    def __check_availability_request(self, performance_id, user_commission):
        """
        This is private method for check availability details.
        """
        # client = Client('demo', 'demopass')
        return self.client.get_availability(performance_id, user_commission)

    def check_availability(self, performance_id='7AB-8', no_of_seats=4, tic_type='Dress Circle', price_band='B'):
        """
        This method is for check available seats details using performance id.

        no_of_seats	Optional  The number of seats the customer would like. If this is specified then availability will
                              only be shown for price bands with at least that many contiguous seats available.
        Ticket type           for check which pool will be available.
        price_band            for get particular pool from seats.
        """
        # ticket_types, meta = self.__check_availability_request(performance_id, no_of_seats=no_of_seats)
        # if tic_type == 'stalls':

        # client = Client('demo', 'demopass')
        ticket_type, meta = self.client.get_availability(performance_id, no_of_seats, tic_type, price_band)
        price_bands, meta = self.client.get_availability(performance_id, no_of_seats, tic_type, price_band)
        #   print(ticket_types)
        #  print(price_bands)

        # for ticket_type in ticket_types:
        print('ticket_type Data:', str(ticket_type))
        print('Price band:', str(price_bands))
        print('Meta Data: %s', meta)
        print('\n')
        # return HttpResponse({
        #   'success': True,
        #  'ticket_type': ticket_type,
        # 'price_band': price_bands,
        # })

    """

    def check_availability(self, performance_id='6IF-D83', no_of_seats=1):
    
        # This method is for check available seats details using performance id.
        
        ticket_types, meta = self.__check_availability_request(performance_id, no_of_seats=no_of_seats)
        for ticket_type in ticket_types:
            print('ticket_type Data:', str(ticket_type))
            print('Meta Data: %s', meta)
            print('\n')
        return HttpResponse({
            'success': True,
            'ticket_type': ticket_types,
        })
    """

    def check_seat_block(self, performance_id='6IF-D83', seat_blocks=True):
        """
        This method is for check individual seats details using performance id.

        seat_blocks	    Include to retrieve individual seats (if, for example, you wish to offer seat selection to
                        your customer). The inclusion of this parameter does not guarantee that individual seat data
                        will be returned - this also depends on (a) whether the event is seated and (b) whether the
                        supplier system supports seat selection.
        """
        # client = Client('demo', 'demopass')
        ticket_types, meta = self.__check_availability_request(performance_id, seat_blocks=seat_blocks)
        print(ticket_types)
        # return HttpResponse({
        #  'success': True,
        # 'ticket_type': ticket_types,
        # })

    def check_example_seat(self, performance_id='6IF-D83', example_seats=True):
        """
        This method is for get example seats details using performance id.

        example_seats	    Include to retrieve example seats. These can be displayed alongside the ticket options when
                            presenting availability to customers. The inclusion of this parameter does not guarantee
                            that example seats data will be returned - this also depends on (a) whether the event is
                            seated and (b) whether the supplier system returns seats at availability time
        """
        ticket_types, meta = self.__check_availability_request(performance_id, example_seats=example_seats)
        print(ticket_types)
        # return HttpResponse({
        #   'success': True,
        #  'ticket_type': ticket_types,
        # })

    def check_commission(self, performance_id='6IF-D83', user_commission=True):
        """
        This method is for check commission details using performance id.

        req_predicted_commission	Include to retrieve commission data. For most partners this will include
                                    predicted_user_commission only (the predicted amount you earn per ticket). Some
                                    partners will also see predicted_gross_commission, which is the total commission
                                    available to be shared between Ingresso and our partner. By default you will see
                                    predicted_user_commission only - if you think you need to see predicted_gross_
                                    commission as well then please get in touch.
        """
        ticket_types, meta = self.__check_availability_request(performance_id, user_commission=user_commission)
        print(ticket_types)
        # return HttpResponse({
        #   'success': True,
        #  'ticket_type': ticket_types,
        # })

    def check_discounts(self, performance_id='6IF-D83', discounts=True):
        """
        This method is for get discount details using performance id.
        A discount represents a price type or concession that is available for a set of available tickets.
        """
        ticket_types, meta = self.__check_availability_request(performance_id, discounts=discounts)
        print(ticket_types)
        # return HttpResponse({
        #   'success': True,
        #  'ticket_type': ticket_types,
        # })

    def get_all_discounts(self, performance_id='6IF-B0O', price_band_code='STALLS', ticket_type_code='A/pool'):
        """
        This method is for get all discount list.

        perf_id	            The performance identifier.
        price_band_code	    The price band code you want to view discounts for.
        ticket_type_code	The ticket type code you want to view discounts for.
        """
        client = Client('demo', 'demopass')
        discounts, meta = client.get_discounts(performance_id, price_band_code, ticket_type_code)
        print('discounts:', discounts)
        # return HttpResponse({
        #   'success': True,
        #  'discounts': discounts,
        # })

    def reserve_ticket(self, performance_id='7AB-8', ticket_type_code='STALLS', price_band_code='B/pool',
                       number_of_seats=2):
        """
        no_of_seats	    The number of tickets you want to reserve.
        perf_id	        The performance identifier for the tickets that you want to reserve.
        price_band_code	The price band identifier for the tickets that you want to reserve.
        ticket_type_code	The ticket type identifier for the tickets that you want to add to your trolley.

        :return status reserved
        """
        # client = Client('demo', 'demopass')

        reservation, meta = self.client.make_reservation(
            performance_id=performance_id,
            ticket_type_code=ticket_type_code,
            price_band_code=price_band_code,
            number_of_seats=number_of_seats,

        )
        print('reservation:', reservation)
        # data = requests.post('https://api.ticketswitch.com/f13/reserve.v1/U-F7C3ECF0-3F33-11EA-999D-AC1F6B466128'
        # '-49F3A031-LDNX', timeout=10)
        # print("post timeout",data)
        if reservation.status == 'reserved':
            print(reservation)
            # return HttpResponse({
            #   'success': True,
            #  'reservation': reservation
            # })
        else:
            print('not able to reserve, some one has taken this seats')

            # return HttpResponse({
            #   'success': False,
            #  'error': 'not able to reserve, some one has taken this seats',
            # })

    """
    def reserve_ticket(self, performance_id='6IF-A7N', ticket_type_code='CIRCLE', price_band_code='C/pool',
                       number_of_seats=3, discounts=['ADULT', 'CHILD', 'CHILD']):
        
        # This method is for get all discount list .
        
        client = Client('demo', demopass)

        reservation, meta = client.make_reservation(
            performance_id=performance_id,
            ticket_type_code=ticket_type_code,
            price_band_code=price_band_code,
            number_of_seats=number_of_seats,
            discounts=discounts)
        print('reservation:', reservation)
        if reservation.status == 'reserved':
            return HttpResponse({
                'success': True,
                'reservation': reservation
            })
        return HttpResponse({
            'success': False,
            'error': 'not able to reserve, some one has taken this seats',
        })
    """

    def reserve_ticket_for_specific_seats(self, performance_id='7AB-8', ticket_type_code='STALLS',
                                          price_band_code='B/pool', number_of_seats=2, seats=("C3", "C4")
                                          ):
        """
        This method is for get reserving specific seats.
        perfomance id for show reserve ticket.
        ticket_type _code for get ticket type with code.
        total for purchase.
        price_band_code for price_band.
        number_of_seats for how many seats you want reserved.
        seats for reserved particular seats that you want.
        """
        # client = Client('demo', 'demopass')

        reservation, meta = self.client.make_reservation(

            performance_id=performance_id,
            ticket_type_code=ticket_type_code,
            price_band_code=price_band_code,
            number_of_seats=number_of_seats,
            seats=seats,
        )
        print('reservation:', reservation.status)
        print('seats:', seats)
        print('number of seat is:', number_of_seats)

        if reservation.status == 'reserved':
            print(reservation)
            # return HttpResponse({
            #   'success': True,
            #  'reservation': reservation
            # })
        else:
            print('not able to reserve, some one has taken this seats')

            # return HttpResponse({
            #   'success': False,
            #  'error': 'not able to reserve, some one has taken this seats',
            # })

    """

    def reserve_ticket_for_specific_seats(self, performance_id='7AB-5', ticket_type_code='STALLS',
                                          price_band_code='A/pool', number_of_seats=2, seats=['A1', 'A2']):
        
        # This method is for get reserving specific seats.
        
        client = Client('demo', demopass)

        reservation, meta = client.make_reservation(
            performance_id=performance_id,
            ticket_type_code=ticket_type_code,
            price_band_code=price_band_code,
            number_of_seats=number_of_seats,
            seats=seats)
        print('reservation:', reservation.status)

        if reservation.status == 'reserved':
            return HttpResponse({
                'success': True,
                'reservation': reservation
            })
        return HttpResponse({
            'success': False,
            'error': 'not able to reserve, some one has taken this seats',
        })
    """

    def hello(self):
        print("Sorry, Time is over. Ticket will be released")

    def reserve_ticket_for_previously_added_in_trolly(self,
                                                      token="BQNWgUBQZU4E-Mp0T-9QvV3LPLiJGYUHZ4iDtz69pIVxkt6gssoUho5ksXkjy5BzXGGiMfXMQgGpqhleHh3TOmU53z1bEGDCKR3miltWZUHKjiKbSE-nqHIWLSCm7IZQxWDMVpqo4IRy3EaUs5dpXBDO8SbKykHKNkXEIQ2V9aRnVH"
                                                            "JNtMxZGet_RAAUsbHTxmj025T6RE724G9BUJW1P0c2UzJZdN8jwSkjBEv-7Q3NdsdSiVbSkReqjqVsXI-MBnGiU0L3_PIc3lVBsyhT4bGxJelSEFNtC7QOlaO6Emr9k7dvmzLTyQj2f1_Jr9UXbYzfBsEAOJOL3xe6tv-VuJTzMHBUd3x-5XGPHrnpHou"
                                                            "YPJ-eCICCNTYTSwdHZNyVSVpAyg7PDFz2jB0cyTstuyXbqVlM5s3nOPwk56JVm6_iVqK1RlYPkbCAWryy2l6ZdA4PdBB0Gn30kczzaO1KevoC_8z1MWm4igCbVHKSynOhuLUVQRdPgAl1pRLJAgPRU422f3lla4uBo6oBQDa_BZ4oWClAYlbQ8hUcNVqR"
                                                            "8C5ggJeTJ1cOG-cufxRGyIsw1Ak-.Z"):
        """
        This method is for get reserving item that already added in trolly.
        With the use of token we get transaction_uuid.
        """
        # client = Client('demo', 'demopass')
        reservation, meta = self.client.make_reservation(token=token)
        var = reservation.trolley.transaction_uuid
        print(var)
        print(reservation.trolley.transaction_id)

        print('Transaction uuid:', reservation.__dict__)
        print(reservation.status)

        def timeout():
            print("Sorry, Time is over. Ticket will be released")

        t = threading.Timer(120.0, timeout)

        if reservation.status == 'reserved':
            print("time start")
            t.start()
            print("successs")
            print(reservation)
        # return HttpResponse({
        #    'success': True,
        #   'reservation': reservation
        # })
        else:
            t.cancel()
            print('timer cancel')
            # client = Client('demo', 'demopass')
            released = self.client.release_reservation('U-D85E65BA-40ED-11EA-999D-AC1F6B466128-832DAC35-LDNX')
            print(released)
            print('not able to reserve, Because ticket already released')
            # return HttpResponse({
            #   'success': False,
            #  'error': 'not able to reserve, Because ticket already released',
            # })

    def purchasing_on_credit(self):
        """
            This method is for get purchasing item with credit.

            user_can_use_customer_data	Data protection question - set this to true if the customer has opted in to
                                        receiving marketing emails from you. Most partners manage marketing opt-ins
                                        themselves so do not provide this. Ingresso will never send marketing
                                        communications to your customers based on this parameter.
                                        Optional - it will default to false.
            """
        customer = Customer(
            first_name='Test',
            last_name='Tester',
            address_lines=['Metro Building', '1 Butterwick'],
            town='London',
            county='London',
            post_code='W6 8DL',
            country_code='uk',
            phone='0203 137 7420',
            email='testing@gmail.com',
            user_can_use_customer_data=True,
        )

        status, callout, meta = self.client.make_purchase(
            '61cfd4eb-1f5b-11e7-b228-0025903268dc',
            customer,
            send_confirmation_email=True,
        )

        print('status:', status)
        if status.status == 'purchased':
            print(status)
            # return HttpResponse({
            #   'success': True,
            #  'status': status
            # })
        else:
            print('Somethings went wrong.')

            # set a parameter dictionary that comes from api.
            # return HttpResponse({
            #   'success': False,
            #  'error': 'Somethings went wrong.',
            # })

    def purchasing_on_redirect(self):
        """
        This method is for get purchasing item with redirect_details.

        return_token	        This token must be alphanumeric with -, _ and . also allowed. We recommend that partners
                                generate a UUID for this token.
        return_url	            The URL that your customer should arrive back to after being redirected.
                                The return_token also has to be present in the path for the return_url and no query
                                string parameters are allowed in the return_url.
        client_http_user_agent	The user agent, taken from the headers of your customer’s web browser.
                                This and client_http_accept are required because the payment provider that generates
                                the callout may need to know detail of the browser, for example to serve a
                                mobile-optimised page.
        client_http_accept	The accept content types, from the headers of your customer’s web browser.
        """

        # client = Client('demo', 'demopass')

        customer = Customer(
            first_name='Test',
            last_name='Tester',
            address_lines=['Metro Building', '1 Butterwick'],
            town='London',
            county='London',
            post_code='W6 8DL',
            country_code='uk',
            phone='0203 137 7420',
            email='testing@gmail.com',
            user_can_use_customer_data=True,
        )

        token = uuid.uuid4()

        redirect_details = RedirectionDetails(
            token=token,
            url='https://www.fromtheboxoffice.com/callback/{}'.format(token),
            user_agent='Mozilla/5.0 (X11; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0',
            accept='text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            remote_site='www.fromtheboxoffice.com',
        )

        status, callout, meta = self.client.make_purchase(
            'U-ADBB51E1-4260-11EA-999D-AC1F6B466128-EDAFD16A-LDNX',
            customer,
            payment_method=redirect_details,
            send_confirmation_email=True,
        )

        print('reservation:', status)
        print(self.client.get_reservation("U-ADBB51E1-4260-11EA-999D-AC1F6B466128-EDAFD16A-LDNX"))

        def timeout():
            print("Sorry, Time is over. Ticket will be released, Please select another available seats")

        if status.status != 'completed':
            t = threading.Timer(120.0, timeout)
            t.start()
            print("successs")
        if status.status == 'purchased':

            print("cancel")
            print(status)
            # return HttpResponse({
            #   'success': True,
            #  'status': status
            # })
        else:
            # client = Client('demo', 'demopass')
            released = self.client.release_reservation('U-ADBB51E1-4260-11EA-999D-AC1F6B466128-EDAFD16A-LDNX')
            print(released)
            print('not able to purchase, Because ticket already taken')
            # return HttpResponse({
            #   'success': False,
            #  'error': 'not able to purchase, Because ticket already taken',
            # })

    # Demo URL
    def get_events(self):
        """
        This function is use demo URL for testing purpose.
        """
        url = "https://demo.ticketswitch.com/f13/events.v1"
        querystring = {"keywords": "", "country_code": "uk"}
        headers = {
            'authorization': "Basic ZGVtbzpkZW1vcGFzcw=="
        }
        response = requests.request(
            "GET", url, headers=headers, params=querystring)

        response = response.json()
        print(response["results"]["event"][0])
        print(response["results"]["event"])
        # return HttpResponse(response["results"]["event"])

    def get_availability(self):
        """
        Ticket_type : get ticket description and ticket code.
        price band code : like A, B, C, D
        number of seats : for check that how many seats are available for booking
        :return: Return response for particular perfomance.

        price band code: A/pool
        price band availability: 17
        seat availability: <Seat YS524>
        seat availability2: 5
        seat availability2: 2
        seat availability2: 4

        seat availability: <Seat YS523>
        seat availability2: 5
        seat availability2: 2
        seat availability2: 3

        price band code: B/pool
        price band availability: 9

        """

        # client = Client('demo', 'demopass')
        ticket_types, meta = self.client.get_availability('1ANL6-2', example_seats=True, seat_blocks=True)

        for ticket in ticket_types:
            print('Ticket description:', ticket.__dict__)
            print("ticket code", ticket.code)
            print('\n')

            for price_band in ticket.price_bands:
                print("price band code:", price_band.__dict__)
                print("price band availability:", price_band.availability)
                for ticket_example in price_band.seat_blocks:
                    print("seat availability:", ticket_example.__dict__)
                    for seat in ticket_example.seats:
                        print("seat availability2:", seat)

    #        return HttpResponse(ticket_types)

    def trolly(self):
        """
        Trolly for get token particular perfomance and seats.

        :return:  Token
        """

        # client = Client('demo', 'demopass')

        trolley, meta = self.client.get_trolley(
            performance_id='1ANL6-2',
            ticket_type_code='3496',
            price_band_code='4437/pool',
            number_of_seats=2,
            seats=['A1', 'A2'],
        )
        # res = requests.get(trolley)
        print(trolley)
        print(trolley.transaction_id)
        print(trolley)
        # return HttpResponse(trolley)

    def add_once(self):
        """
        Not use for now
        :return:
        """

        # client = Client(user='demo', password='demopass')
        addon_events, addon_meta = self.client.get_addons(
            token="")
        print(addon_events)

    def release(self):
        """
        Release the seats that we not want and other customer show that seats available.
        :return: True
        """

        # client = Client('demo', 'demopass')
        released = self.client.release_reservation('U-D85E65BA-40ED-11EA-999D-AC1F6B466128-832DAC35-LDNX')
        if released:
            print(released)
        # return HttpResponse(released)
        else:
            print(False)
            # return HttpResponse(False)

    def send_method(self):
        """
        available send methods of Printable eTicket and Post worldwide are shown.
        :return: <SendMethod VOUCH:b'Printable E-Ticket (Test)'>, <SendMethod POST:b'Post Worldwide (Test)'>
        """

        # client = Client('demo', 'demopass')
        send_methods, meta = self.client.get_send_methods('7AB-8')
        print(send_methods)
        # return HttpResponse(send_methods)

    def get_reservation(self):
        """
        For get reserved ticket.
        :return: Minutes left, Current status and reserverd date and time
        """

        # client = Client('demo', 'demopass')
        reservation, meta = self.client.get_reservation(
            transaction_uuid="U-D6BA6D01-41AF-11EA-9740-AC1F6B465FBC-D90F56E5-LDNX",
            # U-54C0F53D-3EA8-11EA-999D-AC1F6B466128-47EDF2D9-LDNX
        )
        if reservation.trolley.transaction_uuid:

            var = reservation.__dict__
            print('minutes left:', var)
            print('status is:', reservation.status)
            print(reservation.reserved_at)
            # res = requests.get("http://U-0D303CBA-3EAA-11EA-A143-AC1F6B4660C8-E5A83BFE-LDNX")
            # print(res)
            print(reservation)
            # return HttpResponse(reservation)
        else:
            print("Seat taken by other. Plz select other seat")
            # return HttpResponse(reservation)

    def get_purchase(self):
        """
        Enter transaction uuid and you will get purchase particular transaction or not
        :return:
        """
        # client = Client('demo', 'demopass')
        purchase = self.client.get_purchase(
            transaction_uuid="U-4E6B60D0-40C1-11EA-B5DA-AC1F6B466120-4975683-LDNX"
        )

        print(purchase.count(purchase))
        print(purchase)
        # return HttpResponse(purchase)

    def get_status(self):
        """
        Current status of transaction_id
        :return: Status of current seat.
        """
        # client = Client('demo', 'demopass')
        status, meta = self.client.get_status(
            transaction_uuid="U-4E6B60D0-40C1-11EA-B5DA-AC1F6B466120-4975683-LDNX"
        )
        print(status)
        print(meta)
        print("Customer:", status)
        print(status.reserve_iso8601_date_and_time)
        print(status.purchase_iso8601_date_and_time)

        # return HttpResponse(status)

    """

    def get_avail(self):
        from pyticketswitch import Client

        client = Client('demo', 'demopass')
        seat, meta = client.get_availability('7AB-8', example_seats=True)
        print(seat)
        return HttpResponse(seat)
    """


a = Ingresso()
a.get_events_list()
# a.get_events_by_eventid('7AB')
# a.get_availability()
# a.check_commission()
print(a)
