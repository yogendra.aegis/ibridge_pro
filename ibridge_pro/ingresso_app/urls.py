from django.urls import path
from .views import Ingresso

urlpatterns = [

    path('reserve_ticket', Ingresso.reserve_ticket),
    path('trolly', Ingresso.trolly),
    path('reserve_ticket_for_previously_added_in_trolly', Ingresso.reserve_ticket_for_previously_added_in_trolly),
    path('send_method/', Ingresso.send_method),
    path('purchasing_on_redirect', Ingresso.purchasing_on_redirect),
    path('release', Ingresso.release),
    path('check_availability', Ingresso.check_availability),
    path('get_availability/', Ingresso.get_availability),
    path('get_reservation/', Ingresso.get_reservation),
    path('getEventList', Ingresso.get_events_list),
    path('eventListByIDWithMedia', Ingresso.get_events_by_id_with_media),
    path('performancesListByEvent', Ingresso.get_performances_list_by_event),
    path('get_performances_by_id', Ingresso.get_performances_by_id),
    path('get_all_discounts', Ingresso.get_all_discounts),
    path('purchasing_on_credit', Ingresso.purchasing_on_credit),
    path('get_events', Ingresso.get_events),
    path('add_ons/', Ingresso.add_once),
    path('eventListByID', Ingresso.get_events_by_eventid),
    path('get_purchase/', Ingresso.get_purchase),
    path('get_status/', Ingresso.get_purchase),
    # path('get_avail/', Ingresso.get_avail),
    path('check_example_seat', Ingresso.check_example_seat)

    # path('timeout', Ingresso.hello)

]
