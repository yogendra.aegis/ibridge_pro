from django.apps import AppConfig


class IngressoAppConfig(AppConfig):
    name = 'ingresso_app'
