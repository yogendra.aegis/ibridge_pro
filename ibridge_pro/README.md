Pyticketswitch
Release v|version|. (:ref:`Installation <install>`)

Python wrapper around Ingresso's F13 API.

Features
Search for events and performances.
Availability details for performances.
Make live reservations.
Purchase tickets.


Installation
Pyticketswitch is distributed via Pypi and is available open source on Github.

Pip install
To install pyticketswitch simply type this into your terminal:

$ pip install pyticketswitch
If you don't have pip installed, this Python installation guide can guide you through the process.

We recomend working in virtual enviroment to isolate your projects requirements from other python programs running on your system. Here is a good guide.

Source code
You can either clone the public repository:

$ git clone git://github.com/ingresso-group/pyticketswitch.git
Or, download the tarball:

$ curl -OL https://github.com/ingresso-group/pyticketswitch/tarball/master
Once you have a copy of the source, you can embed it in your own Python package, or install it into your site-packages easily:

$ python setup.py install
Legacy Versions
The last version of this wrapper that supported the old XML API was version 1.13.0. Any future bug fixes for the XML wrapper will be on the stable/v1.x branch.

To install this version specifically via pip:

$ pip install pyticketswitch==1.13.1
Or, download the tarball:

$ curl -OL https://github.com/ingresso-group/pyticketswitch/archive/1.13.1.tar.gz


Installation
Pyticketswitch is distributed via Pypi and is available open source on Github.

Pip install
To install pyticketswitch simply type this into your terminal:

$ pip install pyticketswitch
If you don't have pip installed, this Python installation guide can guide you through the process.

We recomend working in virtual enviroment to isolate your projects requirements from other python programs running on your system. Here is a good guide.

Source code
You can either clone the public repository:

$ git clone git://github.com/ingresso-group/pyticketswitch.git
Or, download the tarball:

$ curl -OL https://github.com/ingresso-group/pyticketswitch/tarball/master
Once you have a copy of the source, you can embed it in your own Python package, or install it into your site-packages easily:

$ python setup.py install

Quick Start
This page is intended as a quick introduction to pyticketswitch. As such we won't cover all the options and things you can do, just a brief end to end transaction into a test system.

For the purposes of this example we will use the demo credentials:

user_id: demo
password: demopass
If/when you want to play with real products then drop us a line at commercial@ingresso.co.uk

Ensure that you have pyticketswitch :ref:`properly installed <install>` before continuing.

The Client
The pyticketswitch wrapper is focused around the :class:`Client <pyticketswitch.client.Client>`. Begin by importing it, and instantiating it with your credentials:

>>> from pyticketswitch import Client
>>> client = Client('demo', 'demopass')
We will use this client for the rest of this example. If you lose it somehow (for example by closing your terminal), just instantiate a new one and continue where you left off. The client holds no relevant state other than the authentication credentials.

Finding Events
The top level object exposed by the API is the :class:`Event <pyticketswitch.event.Event>` object and the primary way of finding events is :func:`list_events() <pyticketswitch.client.Client.list_events()>`:

>>> events, meta = client.list_events()
>>> events
[
    <Event 6KS: 1-Day Ticket>,
    <Event 6IF: Matthew Bourne's Nutcracker TEST>,
    <Event 6KT: 3-Day Hopper>
    ...
]
Note

:func:`list_events <pyticketswitch.client.Client.list_performances>` is paginated, as such by default the response will only contain 50 results. See :ref:`Pagination <pagination>` for more information.

This method will take a number of additional parameters that filter and expand the results. See :ref:`Searching for an event <event_search>` for more information

The demo account has access to a handful of fake events and performances that should demonstrate most of the common capabilities of the system. For more information see :ref:`Demo user events <demo_events>`

For the rest of this guide we are going to focus on just a single event 6IF: Matthew Bourne's Nutcracker TEST

If you already have and event ID (the 6IF bit) you can query it directly with the :func:`get_event(event_id) <pyticketswitch.client.Client.get_event>` method:

>>> client.get_event('6IF')
<Event 6IF:Matthew Bourne's Nutcracker TEST>
Events have a number of useful attributes from geographic location to critic reviews. See the :class:`Event <pyticketswitch.event.Event>`'s documentation for more details.

For the time being all we need to know about this event is that it :attr:`is_seated <pyticketswitch.client.Client.is_seated>`, it :attr:`has_performances <pyticketswitch.client.Client.has_performances>`, and it :attr:`needs_performance <pyticketswitch.client.Client.needs_performance>` to book.

Performances
To see available performances for a given event we can use the :func:`list_performances(event_id) <pyticketswitch.client.Client.list_performances>` client method:

>>> performances, meta = client.list_performances('6IF')
>>> performances
[<Performance 6IF-A86: 2017-02-03T19:30:00+00:00>,
 <Performance 6IF-A88: 2017-02-05T19:30:00+00:00>,
 ...,
 <Performance 6IF-B1H: 2017-06-02T19:30:00+01:00>]
Note

:func:`list_performances <pyticketswitch.client.Client.list_performances>` is paginated, as such by default the response will only contain 50 results. See :ref:`Pagination <pagination>` for more information.

For the rest of this guide we will focus on the performance furthest from today 6IF-B1H.

Like with events you can use the :func:`get_performance(performance_id) <pyticketswitch.client.Client.get_performance>` method to retrieve a specific performance when you have the performance ID:

>>> client.get_performance('6IF-B1H')
<Performance 6IF-B1H: 2017-06-02T19:30:00+01:00>
Warning

The performance might have passed by the time you read this, if it has then just select another event from the list. Try and make sure it is not a Saturday, as this will break (intentionally) at later stages.

See :class:`Performance <pyticketswitch.performance.Performance>` Documentation for more information on performances. All we need for now is the :attr:`Performance.id <pyticketswitch.performance.Performance.id>` attribute.

Availability
Now that we have an :class:`Event <pyticketswitch.event.Event>` and a :class:`Performance <pyticketswitch.performance.Performance>`, we need to find out what tickets and prices are available:

>>> ticket_types, meta = client.get_availability('6IF-B1H')
>>> ticket_types
[<TicketType CIRCLE: Upper circle>,
 <TicketType STALLS: Stalls>,
 <TicketType BALCONY: Balcony>]
:func:`get_availability <pyticketswitch.client.Client.get_availability>` returns a list of :class:`TicketTypes <pyticketswitch.ticket_type.TicketType>` and an :class:`AvailabilityMeta <pyticketswitch.availability.AvailabilityMeta>` object.

A ticket type can be generally considered to be a part of house, or part of a venue. Each ticket type will contain a list of one or more price bands:

>>> ticket_type = ticket_types[0]
>>> ticket_type.price_bands
[<PriceBand A/pool>, <PriceBand B/pool>, <PriceBand C/pool>]
>>> price_band = ticket_type.price_bands[0]
>>> price_band.availability
6
>>> price_band.combined_price()
35.0
Availability indicates the number of tickets available in this price band.

The combined price is made up of the seatprice (or the facevalue) and the surcharge (or booking fee) of a ticket.

The meta object contains aggregate information relevant to the ticket types and their children. For example it contains information on the currency the tickets are priced in and what are valid quantities available:

>>> meta.valid_quantities
[2, 3, 4, 5]
>>> meta.currency
<Currency gbp>
Our event is seated but we are unable to reserve individual seats. However some events do allow this, see :ref:`requesting seat availability <seated_availability>` for more information.

For now all we need to continue is a :attr:`TicketType.code <pyticketswitch.ticket_type.TicketType.code>` and a :attr:`PriceBand.code <pyticketswitch.price_band.PriceBand.code>` so pick one of each.